Categories:Office
License:Apache2
Web Site:https://github.com/j4velin/Pedometer/blob/HEAD/README.md
Source Code:https://github.com/j4velin/Pedometer
Issue Tracker:https://github.com/j4velin/Pedometer/issues
Donate:http://www.j4velin.de/donate.php

Auto Name:Pedometer
Summary:Count your steps
Description:
Lightweight pedometer using the hardware step-sensor for minimal battery
consumption. This app is designed to be kept running all the time without
having any impact on your battery life! It uses the hardware step detection
sensor of the Nexus 5, which is already running even when not using any
pedometer app. Therefore the app does not drain any additional battery.
Unlike other pedometer apps, this app does not track your movement or
your location so it doesn't need to turn on your GPS sensor (again: no
impact on your battery).
.

Repo Type:git
Repo:https://github.com/j4velin/Pedometer

Build:1.4,140
    commit=3c65680da9898d4c915e7228945e493ccbcc6588
    gradle=fdroid
    prebuild=sed -i -e '/playCompile/d' build.gradle && \
        touch key.properties && sed -i -e '/debug {/,+4d' build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:140

