Categories:Children,Science & Education
License:Apache2
Web Site:http://sugarizer.org
Source Code:https://github.com/llaske/Sugarizer
Issue Tracker:http://bugs.sugarlabs.org

Auto Name:Sugarizer
Summary:Use Sugar learning platform
Description:
Developed for the "One Laptop Per Child" project, Sugar is a leading
learning platform used by nearly 3 million children around the world.

Sugarizer allow you to discover the Sugar Learning Platform from your
smartphone or tablet. It includes a bunch of educative activities
coming from Sugar.
.

Repo Type:git
Repo:https://github.com/llaske/sugarizer_android_build.git

Build:0.6.0,600
    commit=v0.6

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.6.0
Current Version Code:600
